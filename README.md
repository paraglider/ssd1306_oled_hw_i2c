# ssd1306_oled_hw_i2c

A minimalistic adaptation of Neven Boyanov�s SSD1306 library (http://tinusaur.org) for ATmega328P-based boards. 

**Relocated to https://github.com/ex-punctis/SSD1306_OLED_HW_I2C (new version)**

If you are interested in version 1, I no longer have it, but you can find an improved version of v1 at https://github.com/greenonline/SSD1306_OLED_HW_I2C_LIB